import QtQuick 2.4
import Ubuntu.Components 1.3

import "../components"

Page {
    id: changelog

    property bool startup: false

    header: PageHeader {
        title: i18n.tr("Changelog")

        trailingActionBar {
            numberOfSlots: 1
            actions: [
                Action {
                    iconName: "close"
                    text: i18n.tr("Close")
                    onTriggered: finished()
                    visible: startup
                }
            ]
        }
    }


    signal finished()

    onFinished: {
        console.log("[LOG]: Changelog closed")
        main.settings.version = main.version
        stack.pop()
        stack.push(mainTabs)
    }

    Flickable {
        anchors {
            top: parent.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        contentWidth: parent.width
        contentHeight: column.height

        Column {
            id: column
            width: parent.width

            ChangelogEntry {
                version: "0.6.0"
                comment: i18n.tr("The app has been updated to work with the v2 API of football-data.org. The players of a team can be fetched and the different groups of a competition can be seen.")
                changes: [i18n.tr("New maintainer"), i18n.tr("New team players view"), i18n.tr("New groups competition view")]
            }

            ChangelogEntry {
                version: "0.5.6"
                comment: i18n.tr("With most current seasons being scheduled, I decided to remove the old ones so that there is less clutter in the app.\
If you wish to have access to old seasons too, let me know and I'll see what I can do.\n\
Cheers, Moritz)"
                changes: [i18n.tr("Removed old seasons"),i18n.tr("Minor fixes")]
            }

            ChangelogEntry {
                version: "0.5.5"
                comment: i18n.tr("I've created a small survey to get some basic data on your usage of the app. It really takes only 5 minutes and helps me a lot.\n\
Thanks for your support, Moritz)"
                changes: [i18n.tr("Changed framework to 15.04.5"),i18n.tr("Fixed settings page")]
            }

            ListItem {
                height: layout.height + (divider.visible ? divider.height : 0)
                ListItemLayout {
                    id: layout
                    title.text: "To the survey"
                    title.color: UbuntuColors.green
                    Icon {
                        name: "external-link"
                        SlotsLayout.position: SlotsLayout.Trailing;
                        width: units.gu(2)
                    }
                }
                onClicked: Qt.openUrlExternally("http://goo.gl/forms/1MiCrSDEKBx2Ia8i2")
            }

            ChangelogEntry {
                version: "0.5.4"
                changes: [i18n.tr("Fixes for EURO 2016"), i18n.tr("Added Dutch translation")]
            }

            ChangelogEntry {
                version: "0.5.3"
                changes: [i18n.tr("EURO 2016 integration")]
            }

            ChangelogEntry {
                version: "0.5.2"
                changes: [i18n.tr("Minor bug fixes")]
            }

            ChangelogEntry {
                version: "0.5.1"
                changes: [i18n.tr("Minor bug fixes")]
            }

            ChangelogEntry {
                version: "0.5"
                changes: [i18n.tr("Code cleanup"),
                            i18n.tr("UI refinements")]
            }

            ChangelogEntry {
                version: "0.4.2"
                changes: [i18n.tr("Fixed app crashing with many favorites"),
                            i18n.tr("Modified signalling when editing favorites")]
            }

            ChangelogEntry {
                version: "0.4.1"
                changes: [i18n.tr("Bugfix for empty fixture view"),
                            i18n.tr("Added Greek, updated other translations")]
            }

            ChangelogEntry {
                version: "0.4"
                comment: i18n.tr("I messed up with the database. This shouldn't happen, \
but as you know this app is still in beta. Please delete \
your local files via the settings page, if you updated from an older version. \
Have fun and keep sending feedback.\n\
Thanks for your support, Moritz")
                changes: [i18n.tr("Added changelog viewer"),
                            i18n.tr("UI improvements"),
                            i18n.tr("Fixed some bugs, where UI components weren't refreshed"),
                            i18n.tr("Redesigned matchday selection"),
                            i18n.tr("Optimized fetching of initial data"),
                            i18n.tr("Added manual refreshing for the start page"),
                            i18n.tr("Added French and Esperanto. Thanks to the translators!")]
            }
        }
    }
}

