/*
 *  TeamOverview displays the current fixture, last fixtures and the league table(s) of a team.
 *  It is possible to get to other team's overviews by clicking on the corresponding table entry.
 *  Thus it is possible to have several instances of this class active.
 *  There is an entry for viewing all available fixtures (TeamFixtureView).
 */
import QtQuick 2.4
import Ubuntu.Components 1.3

import "../components"
import "../ModelUtils.js" as ModelUtils

Flickable {
    id: teamFixtureView

    contentWidth: width
    contentHeight: column.height

    clip: true

    Column {
        id: column
        width: parent.width

        ListModel {
            id: nextGameModel
        }

        Repeater {
            id: nextGameView
            model: nextGameModel

            delegate: FixtureDelegate { inlineDate: true }
        }

        ListModel {
            id: lastGamesModel
        }

        Item {
            id: gameIndicator
            width: root.width
            height: indicatorRow.height

            Row {
                id: indicatorRow
                width: parent.width

                Repeater {
                    model: lastGamesModel
                    delegate: Rectangle {
                        height: units.gu(0.75)
                        width: root.width / lastGamesModel.count
                        color: ModelUtils.getGameResultColor(teamId, homeTeamId, homeTeamGoals, awayTeamId, awayTeamGoals)
                    }
                }
            }
        }

        Column {
            width: parent.width

            Repeater {
                id: lastGamesView
                model: lastGamesModel

                FixtureDelegate {}
            }

        }

        ListItem {
            height: layout.height + divider.height
            onClicked: {
                // TRANSLATORS: %1 contains a team name. The page shows all fixtures of this team.
                stack.push(Qt.resolvedUrl("TeamFixtureView.qml"), {"teamId": root.teamId, "title": i18n.tr("%1 - All fixtures").arg(root.title)});
            }
            ListItemLayout {
                id: layout
                title.text: i18n.tr("View all fixtures")
                title.font.weight: Font.Normal
                ProgressionSlot {}
            }
        }

        ListItem {
            height: layout.height + divider.height
            onClicked: {
                stack.push(Qt.resolvedUrl("TeamPlayers.qml"), {"teamId": root.teamId, "title": i18n.tr("%1 - Players").arg(root.title)});
            }
            ListItemLayout {
                id: layout2
                title.text: i18n.tr("View the team players")
                title.font.weight: Font.Normal
                ProgressionSlot {}
            }
        }

        ListModel {
            id: tableModel
        }

        Column {
            id: tableColumn
            width: parent.width

            Repeater {
                id: tableView
                model: tableModel

                delegate: TablePreviewDelegate { teamId: root.teamId; width: tableColumn.width }
            }
        }
    }

    function refreshModel() {
        console.log("[LOG] Refresh team overview");

        lastGamesModel.clear();
        var lastFixtures = datahandler.getTeamFixtures(root.teamId, ModelUtils.TimeFrame.PAST);

        for(var i = 0; i < 5 && i < lastFixtures.length; i++) {
            var fixture = lastFixtures.item(i);
            lastGamesModel.append(ModelUtils.getFixtureModelData(fixture, datahandler.getTeam(fixture.homeTeamId), datahandler.getTeam(fixture.awayTeamId)));
        }

        var nextFixture = datahandler.getTeamFixtures(root.teamId, ModelUtils.TimeFrame.CURRENT);

        nextGameModel.clear();
        fixture = nextFixture.item(0);
        if (fixture)
            nextGameModel.append(ModelUtils.getFixtureModelData(fixture, datahandler.getTeam(fixture.homeTeamId), datahandler.getTeam(fixture.awayTeamId)));


        tableModel.clear();
        for (i = 0; i < root.leagueIds.length; i++) {
            if (datahandler.getLeagueTable(root.leagueIds[i]).length > 0) {
                tableModel.append({"leagueId": root.leagueIds[i]});
            }
        }
    }
}
