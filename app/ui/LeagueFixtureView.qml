import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import "../components"
import "../ModelUtils.js" as ModelUtils


Item {
    property int matchday
    property int numberOfMatchdays

    onMatchdayChanged: {
        datahandler.updateFixtures(page.leagueId, matchday);
    }

    ListView {
        id: fixtureListView

        clip: true
        anchors.fill: parent

        model: ListModel {
            id: fixtureModel
            property bool refreshing: false
        }

        PullToRefresh {
            refreshing: fixtureModel.refreshing
            onRefresh: {
                console.log("REFRESH STARTED")
                fixtureModel.refreshing = true;
                datahandler.updateFixtures(leagueId, matchday);
            }
        }

        section.property: "time"
        section.criteria: ViewSection.FullString
        section.delegate: SectionHeaderDelegate { words: ModelUtils.niceDateTime(section) }

        delegate: FixtureDelegate {}
    }

    function refreshModel() {
        console.log("[LOG] Refresh fixture model")
        fixtureModel.clear();
        var fixtures = datahandler.getMatchdayFixtures(page.leagueId, matchday);

        for(var i = 0; i < fixtures.length; i++) {
            var fixture = fixtures.item(i);
            fixtureModel.append(ModelUtils.getFixtureModelData(fixture, datahandler.getTeam(fixture.homeTeamId), datahandler.getTeam(fixture.awayTeamId)));
        }
        fixtureModel.refreshing = false;
    }
}
