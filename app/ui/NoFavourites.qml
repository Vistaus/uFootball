import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    id: empty
    anchors.fill: parent

    Label {
        id: text
        text: i18n.tr("Add your favourite teams to have the most important information on the start page.")
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        fontSize: "large"
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: units.gu(4)
        }
    }

    Item {
        anchors {
            top: text.bottom
            bottom: button.top
            left: parent.left
            right: parent.right
            margins: units.gu(2)
        }

        Image {
            id: image
            source: "image://thumbnailer/" + Qt.resolvedUrl("../graphics/wimpel.svg")
            anchors.fill: parent
            sourceSize: Qt.size(720, 720)
            fillMode: Image.PreserveAspectFit
            asynchronous: true
            antialiasing: true
        }
    }

    Button {
        id: button
        text: i18n.tr("Add your favorite teams")
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: units.gu(2)
        }
        color: UbuntuColors.green

        onClicked: stack.push(Qt.resolvedUrl("../settings/ManageYourTeams.qml"), {"add": true})
    }
}
