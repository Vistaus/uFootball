import QtQuick 2.4
import QtQml.Models 2.1
import Ubuntu.Components 1.3

import Ubuntu.Components.Themes 1.3

import "../components"
import "../ModelUtils.js" as ModelUtils

Page {
    id: page
    title: "uFootball"

    Component.onCompleted: {
        datahandler.favoritesChanged.connect(yourTeamsView.setTeams)
        datahandler.favoritesChanged.connect(yourTeamsView.updateTeamInformation)
        datahandler.favoritesChanged.connect(yourTeamsView.refresh)

        datahandler.teamFixturesUpdated.connect(yourTeamsView.refresh)
        datahandler.leagueTableUpdated.connect(yourTeamsView.refresh)

        datahandler.timeFrameFixturesUpdated.connect(startFixtureView.refreshModel)
        datahandler.teamsUpdated.connect(startFixtureView.refreshModel)

        pageListView.currentIndex = 0
    }

    TabList {
        id: tabList
    }

    header: PageHeader {
        title: page.title

        leadingActionBar {
            numberOfSlots: 0
            actions: tabList.actions
        }

        extension: Row {
            id: tabs
            width: parent.width
            height: children[0].height
            anchors {
                top: parent.top
                left: parent.left
            }
            z: 1

            TabItem {
                text: i18n.tr("Your teams")
                index: 0
                view: pageListView
            }

            TabItem {
                text: i18n.tr("Fixtures")
                index: 1
                view: pageListView
            }
        }
    }

    DividingShadow {
        width: page.width
        anchors {
            top: page.header.bottom
            left: parent.left
        }
    }

    ObjectModel {
        id: pageModel

        YourTeamsView {
            id: yourTeamsView

            height: pageListView.height
            width: page.width
        }

        StartFixtureView {
            id: startFixtureView

            height: pageListView.height
            width: page.width
        }
    }

    ListView {
        id: pageListView
        model: pageModel

        width: parent.width
        anchors {
            top: page.header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        clip: true
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        highlightMoveDuration: UbuntuAnimation.BriskDuration
        highlightRangeMode: ListView.StrictlyEnforceRange
    }
}
