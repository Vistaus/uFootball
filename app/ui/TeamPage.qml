/*
 *  TeamPage currently only funtions as a wrapper for TeamOverview, providing the possibility for multiple tabs.
 *
 *  TODO:
 *  - rethink the relevance of this class in the near future; may be unnecessary overhead
 */
import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQml.Models 2.1
import QtGraphicalEffects 1.0

import "../components"
import "../ModelUtils.js" as ModelUtils

Page {
    id: root
    property int teamId
    property var leagueIds: []
    property var team
    property bool isFavorite
    property bool favoriteChanged: false

    property Flickable view

    Component.onCompleted: {
        team = datahandler.getTeam(teamId);

        title = team.name;
        isFavorite = datahandler.isFavorite(teamId);

        var rsLeagueIds = datahandler.getTeamLeagues(teamId);
        for (var i = 0; i < rsLeagueIds.length; i++) {
            leagueIds.push(rsLeagueIds.item(i).leagueId);
        }

        view = fixtureView

        datahandler.teamFixturesUpdated.connect(fixtureView.refreshModel);

        datahandler.updateTeamFixtures(teamId);
        datahandler.fetchSquad(teamId);

        fixtureView.refreshModel();
    }

    Component.onDestruction: {
        datahandler.teamFixturesUpdated.disconnect(fixtureView.refreshModel)
        if (favoriteChanged)
            datahandler.favoritesChanged();
    }

    header: PageHeader {
        title: root.title

        trailingActionBar {
            numberOfSlots: 1
            actions: [
                Action {
                    iconName: isFavorite ? "like": "unlike"
                    text: isFavorite ? i18n.tr("Like"): i18n.tr("Unlike")
                    onTriggered: {
                        isFavorite ? datahandler.removeFavorite(teamId) : datahandler.addFavorite(teamId);
                        isFavorite = !isFavorite;
                        favoriteChanged = true;
                    }
                }
            ]
        }
    }

    TabList {
        id: tabList
    }

    Item {
        id: imageWrapper
        width: parent.width
        height: root.height / 8
        clip: true

        anchors.top: root.header.bottom

        Rectangle {
            id: image
            width: parent.width
            height: parent.height

            Image {
                id: background
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
                source: team.icon ? team.icon : "../graphics/default_club_logo.svg" 
                asynchronous: true
                sourceSize: Qt.size(width, height)
                cache: true
            }

            FastBlur {
                anchors.fill: background
                source: background
                cached: true
                radius: 48
            }

            Rectangle {
                id: rect
                height: parent.height
                width: height
                color: "#FFF"

                anchors {
                    top: parent.top
                    left: parent.left
                    leftMargin: units.gu(2)
                }

                Image {
                    anchors.fill: parent
                    anchors.margins: units.gu(0.5)
                    fillMode: Image.PreserveAspectFit
                    source: ModelUtils.useThumbnailer(team.icon, team.iconState)
                    asynchronous: true
                    sourceSize: Qt.size(width, height)
                }
            }
        }
    }

    Rectangle {
        width: parent.width
        height: units.gu(1)
        opacity: 0.3

        gradient: Gradient {
            GradientStop { position: 0.0; color: UbuntuColors.lightGrey}
            GradientStop { position: 1.0; color: "transparent" }
        }

        anchors {
            top: imageWrapper.bottom
            left: parent.left
        }
        z: 1
    }

    ObjectModel {
        id: pageModel

        TeamOverview {
            id: fixtureView

            height: pageListView.height
            width: root.width
        }
    }

    ListView {
        id: pageListView

        width: parent.width

        anchors {
            top: imageWrapper.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        model: pageModel
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        interactive: false
        highlightMoveDuration: UbuntuAnimation.BriskDuration
        highlightRangeMode: ListView.StrictlyEnforceRange
    }
}


