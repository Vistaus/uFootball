/*
 *  TeamPlayers displays all team players of one team.
 *  There can only be one instance of this at a time.
 *
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import "../components"
import "../ModelUtils.js" as ModelUtils


Page {
    id: page
    property int teamId
    header: PageHeader {
        title: page.title
    }

    Component.onCompleted: {
        datahandler.squadUpdated.connect(refreshModel);
        refreshModel()
    }
    Component.onDestruction: {
        datahandler.squadUpdated.disconnect(refreshModel)
    }

    Flickable {
        anchors {
            top: parent.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        ListView {
            id: playersListView

            clip: true
            anchors.fill: parent

            model: ListModel {
                id: playerModel
                property bool refreshing: false
            }

            PullToRefresh {
                refreshing: playerModel.refreshing
                onRefresh: {
                    playerModel.refreshing = true;
                    datahandler.fetchSquad(teamId);
                }
            }

            section.property: "time"
            section.criteria: ViewSection.FullString
            section.delegate: SectionHeaderDelegate { words: ModelUtils.niceDateTime(section) }

            delegate: PlayerDelegate {}
        }
    }

    function refreshModel() {

        playerModel.clear();
        var players = datahandler.getSquadTable(page.teamId);

        for(var i = 0; i < players.length; i++) {
            var player = players.item(i);
            playerModel.append(ModelUtils.getSquadTableEntryData(player));
        }
        playerModel.refreshing = false;
    }
}
