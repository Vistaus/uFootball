import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import "../components"
import "../ModelUtils.js" as ModelUtils
    
ListView {
    id: table

    property string group
    property string stage
    onGroupChanged: {
        refreshModel();
    }
    onStageChanged: {
        refreshModel();
    }
    clip: true

    model: ListModel {
        id: tableModel
        property bool refreshing: false
    }

    PullToRefresh {
        refreshing: tableModel.refreshing
        onRefresh: {
            tableModel.refreshing = true;
            datahandler.updateLeagueTable(leagueId)
        }
    }

    delegate: LeagueTableDelegate {}

    function refreshModel() {
        console.log("[LOG] Refresh table model",page.leagueId,stage,group)
        var standings = datahandler.getStageGroupLeagueTable(page.leagueId,stage,group);
        tableModel.clear();
    
        for(var i = 0; i < standings.length; i++) {
            var standing = standings.item(i);
            var team = datahandler.getTeam(standing.teamId);
    
    
            tableModel.append(ModelUtils.getTableEntryData(standing, team, ""));
        }
    
        tableModel.refreshing = false;
    }
}

