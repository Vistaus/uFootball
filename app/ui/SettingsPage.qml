import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3


import "../components"

Page {
    id: page
    title: i18n.tr("Settings")
    header: PageHeader {
        title: page.title

        leadingActionBar {
            numberOfSlots: 0
            actions: tabList.actions
        }
    }

    TabList {
        id: tabList
    }

    Flickable {
        anchors {
            top: parent.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        contentWidth: parent.width
        contentHeight: contentColumn.height

        Column {
            id: contentColumn
            width: parent.width

            ListItem {
                height: units.gu(4)
                Label {
                    text: i18n.tr("General Settings")
                    font.weight: Font.Normal
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
               }
            }

            ListItem {
                onClicked: stack.push(Qt.resolvedUrl("../settings/ManageYourTeams.qml"))
                ListItemLayout {
                    title.text: i18n.tr("Manage your teams")
                    ProgressionSlot {}
                }
            }

            Component {
                 id: dialogbis
                 Dialog {
                     id: dialoguebis
                     title: i18n.tr("Modify your API key (football-data.org)")
                     text: i18n.tr("Get your API key by signin in")
                     TextField {
                         id: apikey
                         objectName: "inputField"
                         focus: true
                         // Avoid need to press enter to make "Ok" button enabled.
                         inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                         text: datahandler.getApiKey()
                     }
                     Button {
                         text: i18n.tr("Save")
                         onClicked: {
                             datahandler.updateApiKey(apikey.text)
                             datahandler.startup();
                             PopupUtils.close(dialoguebis)
                             }
                         color: UbuntuColors.green
                     }
                     Button {
                         text: i18n.tr("Cancel")
                         onClicked: PopupUtils.close(dialoguebis)
                         color: UbuntuColors.red
                     }
                 }
            }

            ListItem {
                onClicked: {
                    PopupUtils.open(dialogbis)
                }
                ListItemLayout {
                    title.text: i18n.tr("Modify your API key (football-data.org)")
                    ProgressionSlot {}
                }
            }

            Component {
                 id: dialog
                 Dialog {
                     id: dialogue
                     title: i18n.tr("Delete local files")
                     text: i18n.tr("All downloaded icons and football data will be deleted! The app will be closed!")

                     Button {
                         text: i18n.tr("Cancel")
                         onClicked: PopupUtils.close(dialogue)
                     }
                     Button {
                         text: i18n.tr("Delete and close")
                         color: UbuntuColors.red
                         onClicked: {
                             fileManager.deleteLocalFiles()
                             Qt.quit()
                         }
                     }
                 }
            }

            ListItem {
                onClicked: {
                    PopupUtils.open(dialog)
                }
                ListItemLayout {
                    title.text: i18n.tr("Delete local files")
                    ProgressionSlot {}
                }
            }

            ListItem {
                height: units.gu(4)
                Label {
                    text: i18n.tr("Misc.")
                    font.weight: Font.Normal
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
               }
            }

            ListItem {
                onClicked: stack.push(Qt.resolvedUrl("../settings/AboutPage.qml"))
                ListItemLayout {
                    title.text: i18n.tr("About")
                    ProgressionSlot {}
                }
            }

            ListItem {
                onClicked: stack.push(Qt.resolvedUrl("../settings/CreditsPage.qml"))
                ListItemLayout {
                    title.text: i18n.tr("Credits")
                    ProgressionSlot {}
                }
            }

            ListItem {
                onClicked: stack.push(Qt.resolvedUrl("../settings/Changelog.qml"))
                ListItemLayout {
                    title.text: i18n.tr("Changelog")
                    ProgressionSlot {}
                }
            }

            ListItem {
                height: units.gu(4)
                Label {
                    text: i18n.tr("Contribute")
                    font.weight: Font.Normal
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
               }
            }

            ListItem {
                onClicked: Qt.openUrlExternally("https://gitlab.com/vucodil/ufootball/issues")
                ListItemLayout {
                    title.text: i18n.tr("Report a bug / Request a feature")
                    ProgressionSlot {}
                }
            }

        }
    }
}
