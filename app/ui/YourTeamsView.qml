/*
 *  YourTeamsView contains a list of favourite teams. Tables and fixtures are updated, when favourites are changed or on startup.
 *  When other components update relevant information about the teams, this view will be notified and refreshed as soon as it's being displayed.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3

import "../components"

Item {
    property var teams: []

    Component.onCompleted: {
        setTeams();
        if (teams.length > 0) {
            updateTeamInformation();
        }
        refresh();
    }

    ListModel {
        id: listModel
        property bool refreshing: false
    }

    ListView {
        anchors.fill: parent
        model: listModel
        delegate: TeamPreviewDelegate {}

        PullToRefresh {
            refreshing: listModel.refreshing
            onRefresh: {
                listModel.refreshing = true;
                updateTeamInformation();
            }
        }
    }

    Loader {
        id: emptyLoader
        anchors.fill: parent
        visible: listModel.count == 0
    }

    function setTeams() {
        teams = [];
        var favorites = datahandler.getFavorites();
        for (var i = 0; i < favorites.length; i++) {
            teams.push(favorites.item(i).teamId)
        }
    }

    function updateTeamInformation() {
        for (var i = 0; i < teams.length; i++) {
            var leagues = datahandler.getTeamLeagues(teams[i]);
            for (var j = 0; j < leagues.length; j++) {
                datahandler.updateLeagueTable(leagues.item(j).leagueId);
            }
            datahandler.updateTeamFixtures(teams[i]);
        }
    }

    function refresh() {
        listModel.clear();
        for (var i = 0; i < teams.length; i++) {
            listModel.append({"teamId": teams[i]})
        }
        listModel.refreshing = false;

        if (listModel.count == 0) {
            emptyLoader.setSource("NoFavourites.qml");
        }
    }
}
