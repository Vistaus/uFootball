import QtQuick 2.4
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.DownloadManager 1.2
import UFootball 1.0
import "ModelUtils.js" as ModelUtils
import "ui"
import Ubuntu.Components.Popups 1.3

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "ufootball.vucodil"

    id: main

    width: units.gu(50)
    height: units.gu(70)

    Component.onCompleted: {
        // verify if an api key is set
        if (datahandler.getApiKey() == null) {
            // link to the dialogbis from the SettingsPage.qml in place in copying it here
            PopupUtils.open(dialogbis)
        } else {
            // startup requests
            datahandler.startup()
        }
    }

    readonly property string version: "0.6.0"
    property var settings: Settings {
        property string version: "0"
    }

    DataHandler {
        id: datahandler
    }

    FileManager {
        id: fileManager
    }

    SingleDownload {
        id: imageDownloader
        property var queue: []
        property string downloadingId

        onFinished: {
            var finalLocation = fileManager.saveDownload(path, downloadingId);
            datahandler.setIcon(finalLocation, downloadingId);
            queue.shift();
            if (queue.length > 0) {
                downloadingId = queue[0][0];
                download(queue[0][1]);
            } else {
                downloadingId = "";
            }
        }

        onErrorMessageChanged: {
            console.log(errorMessage, downloadingId)
            queue.shift();
            if (queue.length > 0) {
                downloadingId = queue[0][0];
                download(queue[0][1]);
            } else {
                downloadingId = "";
            }
        }

        function addDownload(teamId, url) {
            if (url && url !== "") {
                queue.push([teamId, url]);
                if (queue.length === 1) {
                    downloadingId = teamId;
                    download(url);
                }
            }
        }
    }

    PageStack {
        id: stack

        Component.onCompleted: {
            if (settings.version !== version) {
                push(Qt.resolvedUrl("settings/Changelog.qml"), {"startup": true})
            } else {
                push(mainTabs)
            }
        }

        Tabs {
            id: mainTabs

            onSelectedTabChanged: {
                if (selectedTab === leaguesListTab) {
                    leaguesListPage.source = Qt.resolvedUrl("ui/LeaguesListPage.qml")
                } else if (selectedTab === settingsTab) {
                    settingsPage.source = Qt.resolvedUrl("ui/SettingsPage.qml")
                }
            }

            Tab {
                id: startPageTab
                title: "uFootball"
                page: Loader {
                    source: Qt.resolvedUrl("ui/StartPage.qml")
                }
            }

            Tab {
                id: leaguesListTab
                title: i18n.tr("Leagues")
                page: Loader {
                    id: leaguesListPage
                }
            }

            Tab {
                id: settingsTab
                title: i18n.tr("Settings")
                page: Loader {
                    id: settingsPage
                }
            }
        }
        Component {
             id: dialogbis
             Dialog {
                 id: dialoguebis
                 title: i18n.tr("Modify your API key (football-data.org)")
                 text: i18n.tr("Get your API key by signin in")
                 TextField {
                     id: apikey
                     objectName: "inputField"
                     focus: true
                     // Avoid need to press enter to make "Ok" button enabled.
                     inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                     text: datahandler.getApiKey() ? datahandler.getApiKey() : ''
                 }
                 Button {
                     text: i18n.tr("Save")
                     onClicked: {
                         datahandler.updateApiKey(apikey.text)
                         // startup requests
                         datahandler.startup()
                         PopupUtils.close(dialoguebis)
                         }
                     color: UbuntuColors.green
                 }
                 Button {
                     text: i18n.tr("Cancel")
                     onClicked: PopupUtils.close(dialoguebis)
                     color: UbuntuColors.red
                 }
             }
        }
    }
}

