import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Thumbnailer 0.1

import "../ModelUtils.js" as ModelUtils

ListItem {
        id: delegate
        width: parent.width

        property bool clickable: true
        property bool compressed: width < units.gu(48)

        anchors {
            left: parent.left
            right: parent.right
        }

        contentItem.anchors {
            leftMargin: compressed ? units.gu(0.5) : units.gu(1)
            rightMargin: compressed ? units.gu(0.5) : units.gu(1)
            topMargin: units.gu(0.5)
            bottomMargin: units.gu(0.5)
        }

        onClicked: {
            if (clickable)
                stack.push(Qt.resolvedUrl("../ui/TeamPage.qml"), {"teamId": id});
        }

        RowLayout {
            anchors.fill: parent
            spacing: compressed ? units.gu(0.5) : units.gu(0.8)

            property string fontSize: "medium"
            property var fontColor: model.id == teamId ? UbuntuColors.green : theme.palette.normal.backgroundText

            Label {
                id: rankLabel
                text: rank
                fontSize: parent.fontSize
                Layout.preferredWidth: units.gu(2)
                horizontalAlignment: Text.AlignHCenter
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                color: parent.fontColor
            }

            Image {
                fillMode: Image.PreserveAspectFit
                source: ModelUtils.useThumbnailer(icon, iconState)
                asynchronous: true
                sourceSize: Qt.size(compressed ? units.gu(4.5) : units.gu(5), units.gu(5))
                Layout.preferredWidth: compressed ? units.gu(4) : units.gu(5)
            }

            Label {
                id: name
                text: team
                fontSize: parent.fontSize
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true
                color: parent.fontColor
            }

            Label {
                text: playedGames
                fontSize: parent.fontSize
                Layout.preferredWidth: units.gu(2)
                horizontalAlignment: Text.AlignLeft
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                color: parent.fontColor
            }

            Label {
                text: compressed ? goalsShort : goals
                fontSize: parent.fontSize
                Layout.preferredWidth: compressed ? units.gu(3.75) : units.gu(6)
                horizontalAlignment: Text.AlignHCenter
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                color: parent.fontColor
            }

            Label {
                text: points
                fontSize: parent.fontSize
                Layout.preferredWidth: units.gu(3)
                horizontalAlignment: Text.AlignRight
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                color: parent.fontColor
            }
        }
    }
