import QtQuick 2.4
import Ubuntu.Components 1.3

import "../ModelUtils.js" as ModelUtils

ListItem {
    color: "#FFF"

    ListItemLayout {
        Image {
            fillMode: Image.PreserveAspectFit
            source: ModelUtils.useThumbnailer(icon, iconState)
            asynchronous: true
            sourceSize: Qt.size(units.gu(5), units.gu(5))
            width: units.gu(5)
            SlotsLayout.position: SlotsLayout.Leading
        }

        title.text: name

        Icon {
            visible: check
            name: "ok"
            width: height
            height: units.gu(2)
            color: UbuntuColors.green
            SlotsLayout.position: SlotsLayout.Trailing
        }
    }
}
