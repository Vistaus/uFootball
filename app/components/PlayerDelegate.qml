import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Thumbnailer 0.1

import "../ModelUtils.js" as ModelUtils

ListItem {
    id: delegate

    property bool compressed: width < units.gu(48)

    contentItem.anchors {
        leftMargin: compressed ? units.gu(0.5) : units.gu(1)
        rightMargin: compressed ? units.gu(0.5) : units.gu(1)
        topMargin: units.gu(0.5)
        bottomMargin: units.gu(0.5)
    }
    RowLayout {
        id: layout
        anchors.fill: parent
        spacing: compressed ? units.gu(0.5) : units.gu(0.8)

        property string fontSize: "medium"
        property var fontColor: model.id == teamId ? UbuntuColors.green : theme.palette.normal.backgroundText

        Image {
            id: contactIcon
            fillMode: Image.PreserveAspectFit
            source: "/usr/share/icons/suru/actions/scalable/contact.svg"
            asynchronous: true
            sourceSize: Qt.size(units.gu(2), units.gu(2))
            horizontalAlignment: Text.AlignRight
            //Layout.preferredWidth: compressed ? units.gu(1) : units.gu(2)
            Layout.preferredWidth: units.gu(2)
        }

        Label {
            id: nameLabel
            text: name
            fontSize: parent.fontSize
            horizontalAlignment: Text.AlignLeft
            anchors {
                verticalCenter: parent.verticalCenter
            }
            Layout.preferredWidth: units.gu(7)
        }

        Label {
            id: positionLabel
            text: position ? position : role
            fontSize: parent.fontSize
            horizontalAlignment: Text.AlignLeft
            anchors {
                verticalCenter: parent.verticalCenter
            }
            Layout.preferredWidth: units.gu(4)
        }

        Label {
            text: ModelUtils.age(dateOfBirth)
            fontSize: parent.fontSize
            horizontalAlignment: Text.AlignLeft
            anchors {
                verticalCenter: parent.verticalCenter
            }
            Layout.preferredWidth: units.gu(4)
        }
        Label {
            text:  ModelUtils.getCountryEmoji(nationality)
            fontSize: parent.fontSize
            horizontalAlignment: Text.AlignLeft
            anchors {
                verticalCenter: parent.verticalCenter
            }
            Layout.preferredWidth: units.gu(1)
        }

    }
}
