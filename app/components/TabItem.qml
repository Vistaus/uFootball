import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    property ListView view
    property int index
    property string text

    width: parent.width / parent.children.length
    height: label.height + units.gu(2)

    MouseArea {
        anchors.fill: parent
        onClicked: view.currentIndex = index
    }

    Label {
        id: label
        text: parent.text
        fontSize: "medium"
        color: view.currentIndex === index ? UbuntuColors.green : theme.palette.normal.backgroundText
        font.weight: Font.Normal

        anchors {
            verticalCenter: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }
    }

    Rectangle {
        width: parent.width
        height: units.gu(0.35)
        color: label.color

        anchors {
            top: label.bottom
            topMargin: units.gu(0.75)
            horizontalCenter: label.horizontalCenter
        }
    }
}
