import QtQuick 2.4
import Ubuntu.Components 1.3

ActionList {
    id: tabList

    children: [
        Action {
            text: "uFootball"
            onTriggered: {
                mainTabs.selectedTabIndex = 0
            }
        },

        Action {
            text: i18n.tr("Leagues")
            onTriggered: {
                mainTabs.selectedTabIndex = 1
            }
        },

        Action {
            text: i18n.tr("Settings")
            onTriggered: {
                mainTabs.selectedTabIndex = 2
            }
        }
    ]
}

