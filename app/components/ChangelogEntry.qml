import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    id: item
    property string version
    property string comment: ""
    property var changes: []

    width: parent.width
    height: column.height

    Column {
        id: column
        width: parent.width
//        spacing: units.gu(2)

        ListItem {
            width: parent.width
            height: versionLabel.height + units.gu(3)

            contentItem.anchors {
                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
                bottomMargin: units.gu(0.5)
                topMargin: units.gu(0.5)
            }

            Label {
                id: versionLabel
                text: version
                fontSize: "large"
                font.weight: Text.Normal
                color: UbuntuColors.orange
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        Item {
            width: parent.width
            height: commentLabel.height + units.gu(4)
            visible: comment !== ""

            Label {
                id: commentLabel
                text: comment
                fontSize: "medium"
                anchors {
                    left: parent.left
                    right: parent.right
                    top: parent.top
                    margins: units.gu(2)
                }
                wrapMode: Text.WordWrap
            }
        }

        Column {
            width: parent.width

            SectionHeaderDelegate { words: i18n.tr("Changes") }

            Repeater {
                model: changes
                delegate: ListItem {
                    height: layout.height
                    ListItemLayout {
                        id: layout
                        title.text: modelData
                    }
                }
            }
        }
    }
}

