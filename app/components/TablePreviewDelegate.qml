import QtQuick 2.4
import Ubuntu.Components 1.3

import "../ModelUtils.js" as ModelUtils

Column {
    id: tableItem
    property int leagueId: model.leagueId
    property int teamId
    property string leagueName
    property int size: 5
//    width: parent.width

    Component.onCompleted: refresh()

    ListItem {
        height: layout.height + divider.height
        onClicked: stack.push(Qt.resolvedUrl("../ui/LeaguePage.qml"), {"leagueId": leagueId})
        ListItemLayout {
            id: layout
            title.text: leagueName
            title.font.weight: Font.Normal
            ProgressionSlot {}
        }
    }

    ListModel {
        id: tableModel
    }

    Column {
        width: parent.width

        Repeater {
            id: tableView
            model: tableModel

            delegate: LeagueTableDelegate {}
        }
    }

    function refresh() {
        tableModel.clear();
        var group = datahandler.getGroupLeagueTeam(leagueId,teamId);
        var stage = datahandler.getStageLeagueTeam(leagueId,teamId);
        var leagueTable = datahandler.getStageGroupLeagueTable(leagueId,stage,group);

        var standing = datahandler.getTableEntry(leagueId, teamId);
        if (standing){
            var rank = standing.rank;

            var start = rank - Math.ceil(size / 2);
            var stop = rank + Math.floor(size / 2);
            if (start < 0) {
                stop -= start;
                start = 0;
            }
            if (stop > leagueTable.length) {
                if (stop - leagueTable.length <= start) {
                    start -= stop - leagueTable.length;
                } else {
                    start = 0;
                }
                stop = leagueTable.length;
            }

            leagueName = datahandler.getLeague(leagueId).name;

            for(var j = start; j < stop; j++) {
                var standing = leagueTable.item(j);
                var team = datahandler.getTeam(standing.teamId);

                tableModel.append(ModelUtils.getTableEntryData(standing, team, leagueName));
            }
        }
    }
}
