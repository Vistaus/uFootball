/*
 *  TeamPreviewDelegate is used in a ListView of favourite teams on the StartPage.
 *  It displays compact information (fixtures, tables) of a team and offers access to the more detailed TeamOverview.
 *
 *  TODO:
 *  - proper signaling needs to be figured out
 *      ~ onTeamFixturesUpdated
 *      ~ onLeagueTableUpdated
 *      ~ onFixturesUpdated
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import Ubuntu.Components 1.3

import "../ModelUtils.js" as ModelUtils

Column {
    id: teamFixtureView

    property int teamId: model.teamId
    property var team

    width: parent.width

    Component.onCompleted: refresh()

    ListItem {
        id: teamName
        anchors {
            left: parent.left
            right: parent.right
        }

        onClicked: stack.push(Qt.resolvedUrl("../ui/TeamPage.qml"), {"teamId": teamId});

        Image {
            id: background
            anchors.fill: parent
            fillMode: Image.PreserveAspectCrop
            source: team.icon
            asynchronous: true
            sourceSize: Qt.size(width, height)
            cache: true
        }

        FastBlur {
            anchors.fill: background
            source: background
            cached: true
            radius: 64
        }

        UbuntuShape {
            id: headerBackground
            height: background.height - units.gu(1)
            width: teamNameLayout.width + units.gu(2)
            color: "#FFF"
            radius: "small"
            aspect: UbuntuShape.DropShadow

            anchors {
                left: parent.left
                leftMargin: units.gu(0.5)
                verticalCenter: parent.verticalCenter
            }

            RowLayout {
                id: teamNameLayout
                anchors {
                    left: parent.left
                    leftMargin: units.gu(1)
                    verticalCenter: parent.verticalCenter
                }
                spacing: units.gu(1.25)

                Image {
                    fillMode: Image.PreserveAspectFit
                    source: ModelUtils.useThumbnailer(team.icon, team.iconState)
                    asynchronous: true
                    sourceSize: Qt.size(headerBackground.height * 0.7, headerBackground.height * 0.7)
                }

                Label {
                    id: nameLabel
                    text: team.name
                    fontSize: "large"
                }
            }
        }
    }

    ListModel {
        id: nextGame
    }

    Repeater {
        id: nextGameView
        model: nextGame

        delegate: FixtureDelegate { inlineDate: true}
    }

    ListModel {
        id: lastGames
    }

    Item {
        id: gameIndicator
        width: teamFixtureView.width
        height: indicatorRow.height

        Row {
            id: indicatorRow
            width: parent.width

            Repeater {
                model: lastGames
                delegate: Rectangle {
                    height: units.gu(0.75)
                    width: teamFixtureView.width / lastGames.count
                    color: ModelUtils.getGameResultColor(teamId, homeTeamId, homeTeamGoals, awayTeamId, awayTeamGoals)
                }
            }
        }
    }

    ListModel {
        id: tableModel
    }

    Column {
        id: tableColumn
        width: parent.width

        Repeater {
            id: tableView
            model: tableModel

            delegate: TablePreviewDelegate { teamId: teamFixtureView.teamId; width: tableColumn.width; size: 3 }
        }
    }

    function refresh() {
        team = datahandler.getTeam(teamId);
        refreshFixtures();
        refreshTables();
    }

    function refreshFixtures() {
        var lastFixtures = datahandler.getTeamFixtures(teamId, ModelUtils.TimeFrame.PAST);

        lastGames.clear();
        for(var i = 0; i < 5 && i < lastFixtures.length; i++) {
            var fixture = lastFixtures.item(i);
            lastGames.append(ModelUtils.getFixtureModelData(fixture, datahandler.getTeam(fixture.homeTeamId), datahandler.getTeam(fixture.awayTeamId)));
        }

        var nextFixture = datahandler.getTeamFixtures(teamId, ModelUtils.TimeFrame.CURRENT);

        fixture = nextFixture.item(0);
        nextGame.clear();
        if (fixture)
            nextGame.append(ModelUtils.getFixtureModelData(fixture, datahandler.getTeam(fixture.homeTeamId), datahandler.getTeam(fixture.awayTeamId)));
    }

    function refreshTables() {
        var leagueIds = [];
        var rsLeagues = datahandler.getTeamLeagues(teamId);
        for (var i = 0; i < rsLeagues.length; i++) {
            leagueIds.push(rsLeagues.item(i).leagueId);
        }

        tableModel.clear();
        for (i = 0; i < leagueIds.length; i++) {
            if (datahandler.getLeagueTable(leagueIds[i]).length > 0) {
                tableModel.append({"leagueId": leagueIds[i]});
            }
        }
    }
}
