include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
)

set(
    UFootballbackend_SRCS
    modules/UFootball/backend.cpp
    modules/UFootball/filemanager.cpp
)

add_library(UFootballbackend MODULE
    ${UFootballbackend_SRCS}
)

set_target_properties(UFootballbackend PROPERTIES
         LIBRARY_OUTPUT_DIRECTORY UFootball)

qt5_use_modules(UFootballbackend Gui Qml Quick)

# Copy qmldir file to build dir for running in QtCreator
add_custom_target(UFootballbackend-qmldir ALL
    COMMAND cp ${CMAKE_CURRENT_SOURCE_DIR}/modules/UFootball/qmldir ${CMAKE_CURRENT_BINARY_DIR}/UFootball
    DEPENDS ${QMLFILES}
)

# Install plugin file
install(TARGETS UFootballbackend DESTINATION ${QT_IMPORTS_DIR}/UFootball/)
install(FILES   modules/UFootball/qmldir DESTINATION ${QT_IMPORTS_DIR}/UFootball/)

